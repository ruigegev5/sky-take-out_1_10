package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();//123456=>md5  rerteryfar23543654623234

        //1、根据用户名查询数据库中的数据 rerteryfar23543654623234
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    @Override
    public void insert(EmployeeDTO employeeDTO) {
        System.out.println("线程id:" + Thread.currentThread().getId());
        //调用一个或者多个mapper方法，实现业务逻辑处理
        Employee employee = new Employee();
        // employee.setName(employeeDTO.getName());
        // employee.setUsername(employeeDTO.getUsername());
        //对象拷贝
        BeanUtils.copyProperties(employeeDTO, employee);

        //2.完善数据
        //对密码做了加密处理
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        employee.setStatus(StatusConstant.ENABLE);
        //创建时间和修改时间
        // employee.setCreateTime(LocalDateTime.now());
        // employee.setUpdateTime(LocalDateTime.now());
        //创建人和修改人
        // employee.setCreateUser(BaseContext.getCurrentId());
        // employee.setUpdateUser(BaseContext.getCurrentId());

        //目标：新增数据到employee表中  insert into employee() values()
        employeeMapper.insert(employee);
    }

    @Override
    public PageResult page(EmployeePageQueryDTO employeePageQueryDTO) {
        //select * from employee limit 0，10;
        //select count(*) from employee ;
        //select * from employee where name
        //1.设置分页参数
        PageHelper.startPage(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());

        //2.调用mapper
        Page<Employee> page = employeeMapper.page(employeePageQueryDTO.getName());

        //目标：返回分页对象pageresult
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void updateStatus(Integer status, Long id) {
        //update employee set status=?,name=?,username=? where id =? 动态修改语句
        // Employee employee = new Employee();
        // employee.setId(id);
        // employee.setStatus(status);
        // employee.setUpdateTime(LocalDateTime.now());
        // employee.setUpdateUser(BaseContext.getCurrentId());

        Employee employee = Employee.builder()
                .id(id)
                .status(status)
                // .updateTime(LocalDateTime.now())
                // .updateUser(BaseContext.getCurrentId())
                .build();

        employeeMapper.updateById(employee);

    }

    @Override
    public Employee getById(Long id) {

        //调用mapper
        Employee employee = employeeMapper.getById(id);
        //密码处理
        employee.setPassword("****");
        return employee;
    }

    @Override
    public void updateById(EmployeeDTO employeeDTO) {
        //update employee set name=?,username=? where i=?
        Employee employee = new Employee();
        //对象拷贝
        BeanUtils.copyProperties(employeeDTO,employee);
        //完善修改信息
        // employee.setUpdateTime(LocalDateTime.now());
        // employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.updateById(employee);
    }

}
